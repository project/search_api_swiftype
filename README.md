# Search API Swiftype

The _Search API Swiftype_ module integrates the _Swiftype_ search engine into
your Drupal site.

## Installation

Simply download the module from [Drupal.org][1] and follow the instructions on
[Installing Drupal 8 Modules][2].

## Limitations

### Sorting

At the moment, _Swiftype_ allows to sort by one field only. Having multiple
sorts in you result list is therefore not possible.

## Contact

### Current maintainers:

* Brian Vuyk [brianv][3]
* Stefan Borchert [stborchert][4]

### Sponsors

  * [Savas Labs][6]
    We craft Drupal web systems that propel organizations
  * [undpaul][5]
    Drupal experts providing professional [Drupal development services][6].

[1]:https://www.drupal.org/project/search_api_swiftype
[2]:https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
[3]:http://drupal.org/user/46854
[3]:http://drupal.org/user/36942
[4]:https://www.undpaul.de
[5]:https://www.drupal.org/undpaul
[6]:https://savaslabs.com/
