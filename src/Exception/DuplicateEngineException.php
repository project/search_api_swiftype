<?php

namespace Drupal\search_api_swiftype\Exception;

/**
 * Defines a DuplicateEngineException.
 */
class DuplicateEngineException extends SwiftypeException {

}
