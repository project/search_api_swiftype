<?php

namespace Drupal\search_api_swiftype\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a generic SwiftypeException.
 */
class SwiftypeException extends \Exception {

  use StringTranslationTrait;

}
