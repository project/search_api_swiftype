<?php

namespace Drupal\search_api_swiftype\Exception;

/**
 * Defines a DocumentTypeNotFoundException.
 */
class DocumentTypeNotFoundException extends SwiftypeException {

}
