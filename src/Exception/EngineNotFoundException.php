<?php

namespace Drupal\search_api_swiftype\Exception;

/**
 * Defines a EngineNotFoundException.
 */
class EngineNotFoundException extends SwiftypeException {

}
