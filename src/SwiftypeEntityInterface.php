<?php

namespace Drupal\search_api_swiftype;

use Drupal\search_api_swiftype\SwiftypeClient\SwiftypeClientInterface;

/**
 * Defines the interface for a Swiftype entity.
 */
interface SwiftypeEntityInterface {

  /**
   * Get the Swiftype client service.
   *
   * @return \Drupal\search_api_swiftype\SwiftypeClient\SwiftypeClientInterface
   *   The Swiftype client service.
   */
  public function getClientService(): SwiftypeClientInterface;

  /**
   * Get internal Id of the entity.
   *
   * @return string
   *   The internal entity Id.
   */
  public function getId(): string;

  /**
   * Get raw entity data.
   *
   * @return array<string>
   *   The raw data of the Swiftype entity.
   */
  public function getRawData(): array;

}
